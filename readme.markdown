# Simple Function Timer

A seriously simple tool that measures how long it took for a function to run a certain number of times. Created as part of SydJS "refactor wars" in 2011 (http://weblog.200ok.com.au/2011/06/one-script-seven-ways.html). Uses window.performance where available, falls back to simple Date-based timing.

This script is just for rough testing. For serious benchmarking, use a serious benchmarking tool. 

For more useful results, run much higher reps and shut down other apps. Very fast js may report as taking zero ms for low repetitions. See also (http://ejohn.org/blog/accuracy-of-javascript-time/)

# Usage

    :::javascript
    simpleFunctionTimer(runWhat,howManyTimes,showResults,logResults);

* runWhat The function to run in the test.
* howManyTimes Optional. The number of times to run the function. Defaults to 1 if not set.
* showResults Optional. ID of element to append results in current page.
* logResults Optional. Default true. Set to false to disable console.log.

# Results

Function returns results:

    :::javascript
    { 
        "funcName": "foo",
        "repetitions": 50,
        "duration": 51.00000000675209,
        "average": 1.0200000001350418,
        "testType": "window.performance",
        "message": "simpleDummy x 50. Duration: 51.00000000675209ms. Average: 1.0200000001350418ms. Tested using window.performance."
    }

Note 0 repetitions means the function was not run, ie. it was skipped.

# Tested

Tested in IE10 (and IE10's emulations of IE7-9), FF 22, Chrome 27, Opera 15.