/*! From https://bitbucket.org/200ok/simplefunctiontimer | Apache 2.0 license */
/**
 * Simple execution time test for JavaScript function. 
 *
 * Very rough, this test is not run in a hermetically sealed lab ;) Influenced by many factors.
 * Very small scripts may return zero execution time. See also http://ejohn.org/blog/accuracy-of-javascript-time/
 *
 * @param {function} runWhat The function to run in the test.
 * @param {number} howManyTimes Optional. The number of times to run the function. Defaults to 1 if not set.
 * @param {string} showResults Optional. ID of element to append results in current page.
 * @param {boolean} logResults Optional. Default true. Set to false to disable console.log.
 * @return {object} Contains function name, duration, etc. Note repetitions=0 means the test was not run.
 */
function simpleFunctionTimer(runWhat,howManyTimes,showResults,logResults) {

    var logging = (typeof console != "undefined" && console.log && logResults !== false);
    var visible = (showResults) ? document.getElementById(showResults) : false;
    var consoleTimer = (typeof console.time != "undefined");
    var windowPerf = (typeof window.performance.now === "function");
    var results = {};

    function runTest() {
        for ( i=0; i<repetitions; i++ ) {
            runWhat();
        }
    }

    if ( typeof(runWhat) === "function" ) {

        var startTime,
            endTime,
            duration,
            average,
            funcName = runWhat.name,
            testType,
            repetitions = (typeof(howManyTimes) === "number") ? howManyTimes : 1,
            i;

        if (windowPerf) {
            startTime = window.performance.now();
            runTest();
            endTime = window.performance.now();
            duration = (endTime - startTime);
            testType = "window.performance";
        } else if (consoleTimer) {
            console.time("simpleFunctionTimer");
            runTest();
            duration = console.timeEnd("simpleFunctionTimer");
            testType = "console.time";
        } else {
            startTime = new Date().getTime();
            runTest();
            endTime = new Date().getTime();
            duration = (endTime - startTime);
            testType = "Date().getTime";
       }

        average = (duration / repetitions);

        results = {
            funcName: funcName,
            repetitions: repetitions,
            duration: duration,
            average: average,
            testType: testType,
            message: funcName + " x " + repetitions + ". Duration: " + duration + "ms. Average: " + average + "ms. Tested using " + testType + "."
        };

    } else {
        results = {
            funcName: "notAFunction",
            repetitions: 0,
            duration: 0,
            average: 0,
            testType: "notTested",
            message: runWhat + " is not a function. Skipped by simpleFunctionTimer."
        };
    }

    if (logging) {
        console.log(results.message);
    }

    if (visible) {
        visible.innerHTML = visible.innerHTML + "<p>" + results.message + "</p>";
    }

    return results;

};